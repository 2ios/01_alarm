# 01_Alarm
---------------

Simple Alarm

# Gif Showcase
---------------

![Alt Text](alarm.gif)

## Authors
---------------

* **Andrei Golban**

## License
---------------

This project is licensed under the MIT License - see the [LICENSE.md](https://opensource.org/licenses/MIT) file for details

