//
//  MenuAlarmViewController.swift
//  Alarm
//
//  Created by User543 on 17.05.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit

class MenuTableAlarmViewController: UITableViewController, RepeatAlarmDelegate, NameAlarmDelegate, SongAlarmDelegate {
    
    var array = [Int]()
    
    @IBOutlet weak var repeatDayViewCell: MenuTableAlarmCell!
    @IBOutlet weak var nameViewCell: MenuTableAlarmCell!
    @IBOutlet weak var songViewCell: MenuTableAlarmCell!
    @IBOutlet weak var repeatSongViewCell: MenuTableAlarmCell!
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier! {
        case "ToRepeatAlarmSegue":
            let repeatVC: RepeatTableAlarmViewController = segue.destination as! RepeatTableAlarmViewController
            repeatVC.delegate = self
            repeatVC.daysArray = array
        case "ToRenameAlarmSegue":
            let renameVC: NameAlarmViewController = segue.destination as! NameAlarmViewController
            renameVC.delegate = self
            renameVC.nameString = nameViewCell.detail.text
        case "ToSongAlarmSegue":
            let songVC: SongTableAlarmViewController = segue.destination as! SongTableAlarmViewController
            songVC.delegate = self
        default:
            break
        }
    }
    
    // MARK: repeatAlarm function of RepeatAlarmDelegate
    func repeatAlarm(string : String, arrayDays: [Int]) -> Void {
        repeatDayViewCell.detail.text = string
        array = arrayDays
    }
    // MARK: renameAlarm function of NameAlarmDelegate
    func nameAlarm(string: String) -> Void {
        nameViewCell.detail.text = string
    }
    
    func songAlarm(string: String) -> Void {
        songViewCell.detail.text = string
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
}
