//
//  SongAlarmViewController.swift
//  Alarm
//
//  Created by User543 on 16.05.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit

protocol SongAlarmDelegate: NSObjectProtocol {
    func songAlarm(string: String) -> Void
}

class SongTableAlarmViewController: UITableViewController {
    
    weak open var delegate: SongAlarmDelegate?
    
    var songName: String = "Не выбран"
    
    // MARK: Table View DataSource
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BuyCell", for: indexPath)
        cell.textLabel?.text = "Ringtone \(indexPath.row)"
        return cell
    }
    // MARK: End of Table View DataSource
    
    // MARK: Table View Delegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        for index in 0..<3 {
            let localIndex = IndexPath(row: index, section: 0)
            let cell = tableView.cellForRow(at: localIndex)
            cell?.accessoryType = .none
        }
        if let cell = tableView.cellForRow(at: indexPath) {
            cell.accessoryType = .checkmark
            if indexPath.row == 0 {
                songName = "ringtone1"
            } else if indexPath.row == 1 {
                songName = "ringtone2"
            } else {
                songName = "ringtone3"
            }
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    // End of Table View Delegate
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        delegate?.songAlarm(string: songName)
        super.viewWillDisappear(animated)
    }
}

