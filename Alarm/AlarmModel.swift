//
//  AlarmModel.swift
//  Alarm
//
//  Created by User543 on 18.05.17.
//  Copyright © 2017 User543. All rights reserved.
//

import Foundation

class AlarmModel: NSObject {
    
    public var time: Date? = nil
    public var name: String = ""
    public var isOnSwitch : Int = 1
}
