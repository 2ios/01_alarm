//
//  ViewController.swift
//  Alarm
//
//  Created by User543 on 16.05.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit
import CoreData
import UserNotifications


class AlarmTableViewController: UITableViewController {
    
    // Allow Delegate Data Core
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    // Initialization of AlarmCoreData
    var alarms: [AlarmData] = []
    
    @IBOutlet weak var editDoneButton: UIBarButtonItem!
    var indexRow: Int = 0
    var editAdd: Bool = false
    
    // Check if barButtonIsPressed and modify editingStyle of Cell
    var editTableViewCellStyle = UITableViewCellEditingStyle.none
    
    func barButtonIsPressed(_ sender: UIBarButtonItem) {
        if editTableViewCellStyle == .none {
            editTableViewCellStyle = .delete
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Готово", style: .plain, target: self, action: #selector(barButtonIsPressed))
        } else {
            editTableViewCellStyle = .none
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Изменить", style: .plain, target: self, action: #selector(barButtonIsPressed))
        }
        tableView.setEditing(editTableViewCellStyle != .none, animated: true)
        tableView.reloadData()
    }

    // MARK: Table View DataSource
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if alarms.count == 0 {
            tableView.separatorStyle = UITableViewCellSeparatorStyle.none
            let noDataLabel: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            noDataLabel.text = "Нет будильников"
            noDataLabel.textColor = UIColor.black
            noDataLabel.textAlignment = .center
            tableView.backgroundView = noDataLabel
        }
        else {
            tableView.separatorStyle = UITableViewCellSeparatorStyle.singleLine
            tableView.backgroundView = nil
        }
        return alarms.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let alarmCell = tableView.dequeueReusableCell(withIdentifier: "AlarmCell", for: indexPath) as! AlarmTableViewCell
        //Show alarms from CoreData on tableView
        let alarm = alarms[indexPath.row]
        alarmCell.titleAlarm.text = alarm.timeAlarm
        alarmCell.detailAlarm.text = alarm.nameAlarm
        alarmCell.switchAlarm.isOn = alarm.turnOnAlarm
        
        if alarm.turnOnAlarm {
            alarmCell.backgroundColor = UIColor.white
        } else {
            alarmCell.backgroundColor = UIColor.gray
        }
        
        alarmCell.switchAlarm.tag = indexPath.row
        alarmCell.switchAlarm.addTarget(self, action: #selector(switchAction), for: .valueChanged)
        if alarm.turnOnAlarm {
            let days = alarm.selectDays as! Array<Int>
            for index in 0..<days.count {
                let date = alarm.timeAlarmDate
                var triggerDate = Calendar.current.dateComponents([.weekday,.hour,.minute], from: date! as Date)
            if days[index] == 6 {
                    triggerDate.weekday = 1
            } else {
                    triggerDate.weekday = days[index] + 2
            }
                let content = UNMutableNotificationContent()
                content.title = alarm.nameAlarm!
                content.body = alarm.timeAlarm!
                content.sound = UNNotificationSound.init(named: "\(String(describing: alarm.songAlarm!)).mp3")
                content.categoryIdentifier = "SnoozeAndDelete"
                let triggertest = UNCalendarNotificationTrigger(dateMatching: triggerDate, repeats: alarm.repeatAlarm)
                let request = UNNotificationRequest(identifier: "\(indexPath.row)+\(triggerDate.weekday!)", content: content, trigger: triggertest)
                UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
            
            let snooze = UNNotificationAction(identifier: "Snooze", title: "Snooze", options: [.foreground])
            let delete = UNNotificationAction(identifier: "Delete", title: "Delete", options: [.destructive])
            let category = UNNotificationCategory(identifier: "SnoozeAndDelete", actions: [snooze, delete], intentIdentifiers: [], options: [])
            UNUserNotificationCenter.current().setNotificationCategories([category])
            }
        }
        
        if editTableViewCellStyle == .delete {
            alarmCell.switchAlarm.isHidden = true
        } else {
            alarmCell.switchAlarm.isHidden = false
        }
        
        return alarmCell
    }
    //  End of Table View DataSource
    
    // MARK: Table View Delegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        indexRow = indexPath.row
        editAdd = true
        for index in 0...6 {
            UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: ["\(indexPath.row)+\(index)"])
        }
        self.performSegue(withIdentifier: "ToAddEditAlarmSegue", sender: nil)
    }
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
    //If delete cell, save data and reload tableview
        if editingStyle == .delete {
            let alarmCell = alarms[indexPath.row]
            context.delete(alarmCell)
            (UIApplication.shared.delegate as! AppDelegate).saveContext()
            do {
                alarms = try context.fetch(AlarmData.fetchRequest())
            } catch {
                print("Неудачная загрузка файлов из CoreData")
            }
            if alarms.count == 0 {
                self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)
            }
        }
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
        tableView.reloadData()
    }
    // End of Table View Delegate
    
    func switchAction (_ sender: UISwitch) {
        let alarm = alarms[sender.tag]
        alarm.turnOnAlarm = sender.isOn
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
        let indexPath = IndexPath(row: sender.tag, section: 0)
        let cell = tableView.cellForRow(at: indexPath)
        if sender.isOn {
            cell?.backgroundColor = UIColor.white
            let days = alarm.selectDays as! Array<Int>
            for index in 0..<days.count {
                let date = alarm.timeAlarmDate
                var triggerDate = Calendar.current.dateComponents([.weekday,.hour,.minute], from: date! as Date)
                if days[index] == 6 {
                    triggerDate.weekday = 1
                } else {
                    triggerDate.weekday = days[index] + 2
                }
                let content = UNMutableNotificationContent()
                content.title = alarm.nameAlarm!
                content.body = alarm.timeAlarm!
                content.sound = UNNotificationSound.init(named: "\(String(describing: alarm.songAlarm!)).mp3")
                content.categoryIdentifier = "SnoozeAndDelete"
                let triggertest = UNCalendarNotificationTrigger(dateMatching: triggerDate, repeats: alarm.repeatAlarm)
                let request = UNNotificationRequest(identifier: "\(indexPath.row)+\(triggerDate.weekday!)", content: content, trigger: triggertest)
                UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
                
                let snooze = UNNotificationAction(identifier: "Snooze", title: "Snooze", options: [.foreground])
                let delete = UNNotificationAction(identifier: "Delete", title: "Delete", options: [.destructive])
                let category = UNNotificationCategory(identifier: "SnoozeAndDelete", actions: [snooze, delete], intentIdentifiers: [], options: [])
                UNUserNotificationCenter.current().setNotificationCategories([category])
            }
        } else {
            cell?.backgroundColor = UIColor.gray
            let days = alarm.selectDays as! Array<Int>
            var triggerDate = 0
            for index in 0..<days.count {
                if days[index] == 6 {
                    triggerDate = 1
                } else {
                    triggerDate = days[index] + 2
                }
                UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: ["\(indexPath.row)+\(triggerDate)"])
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Load AlarmCoreData and Reload TableView
        getData()
        tableView.reloadData()
        editTableViewCellStyle = .none
        tableView.setEditing(false, animated: false)
        editAdd = false
        if alarms.count == 0 {
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)
        } else {
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Изменить", style: .plain, target: self, action: #selector(barButtonIsPressed))
        }
        print(Calendar.current.dateComponents([.weekday,.hour,.minute], from: Date()))
        UNUserNotificationCenter.current().getPendingNotificationRequests { (test: [UNNotificationRequest]) in
            print("Ordere in asteptare=================================================")
            print(test)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let addEditNavigationController = segue.destination as! UINavigationController
        let addEditVC = addEditNavigationController.topViewController as! AddEditAlarmViewController
        addEditVC.editAdd = editAdd
        addEditVC.indexRow = indexRow
        }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: getData function of CoreData
    func getData() {
        do {
            alarms = try context.fetch(AlarmData.fetchRequest())
        } catch {
            print("Неудачная загрузка файлов из CoreData")
        }
    }
    
}

