//
//  RepeatAlarmViewController.swift
//  Alarm
//
//  Created by User543 on 16.05.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit

protocol RepeatAlarmDelegate: NSObjectProtocol
{
    func repeatAlarm(string : String, arrayDays: [Int]) -> Void
}

class RepeatTableAlarmViewController: UITableViewController {
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    // Initialization of AlarmCoreData
    var alarms: [AlarmData] = []
    
    weak open var delegate: RepeatAlarmDelegate?
    
    var daysArray = [Int]()
    var repeatModel = RepeatModel()
    
    // MARK: Table View DataSource
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return repeatModel.items.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RepeatCell", for: indexPath)
        cell.textLabel?.text = repeatModel.items[indexPath.row]
        return cell
    }
    // End of Table View DataSource
    
    // MARK: Table View Delegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) {
            if cell.accessoryType == .none {
                cell.accessoryType = .checkmark
            } else {
                cell.accessoryType = .none
            }
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    // End of Table View Delegate
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        var detailCellString = Array<String>()
        for index in 0..<repeatModel.itemsShort.count {
            let indexPath = IndexPath(row: index, section: 0)
            let cell: UITableViewCell = self.tableView.cellForRow(at: indexPath)!
            if cell.accessoryType == .checkmark {
                daysArray.append(indexPath.row)
                detailCellString.append(repeatModel.itemsShort[index])
            }
        }
        let stringForCell: String = detailCellString.joined(separator: " ")
        delegate?.repeatAlarm(string: stringForCell, arrayDays: daysArray)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        for index in 0..<daysArray.count {
            let indexPath = IndexPath(row: daysArray[index], section: 0)
            let cell = tableView.cellForRow(at: indexPath)
            cell?.accessoryType = .checkmark
        }
        daysArray.removeAll()
    }
}
