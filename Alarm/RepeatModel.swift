//
//  RepeatObject.swift
//  Alarm
//
//  Created by User543 on 17.05.17.
//  Copyright © 2017 User543. All rights reserved.
//

import Foundation

class RepeatModel: NSObject {
    
    public let items = ["Каждый понед.", "Каждый вторник", "Каждую среду", "Каждый четверг", "Каждую пятницу", "Каждую субботу", "Каждое воскр."]
    public let itemsShort = ["Пн", "Вт", "Ср", "Чт", "Пт", "Сб", "Вс"]
    public let itemsGroup = ["Будние дни","Выходные"]
}
