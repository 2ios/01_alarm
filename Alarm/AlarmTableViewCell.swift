//
//  AlarmTableViewCell.swift
//  Alarm
//
//  Created by Andrei Golban on 5/21/17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit

class AlarmTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleAlarm: UILabel!
    @IBOutlet weak var detailAlarm: UILabel!
    @IBOutlet weak var switchAlarm: UISwitch!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
