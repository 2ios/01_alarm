//
//  MenuAlarmTableCell.swift
//  Alarm
//
//  Created by Andrei Golban on 5/21/17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit

class MenuAlarmTableCell: UITableViewCell {

    @IBOutlet weak var daysOfAlarmCell: UITableViewCell!
    @IBOutlet weak var nameAlarmCell: UITableViewCell!
    @IBOutlet weak var songAlarmCell: UITableViewCell!
    @IBOutlet weak var repeatAlarm: UITableViewCell!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
