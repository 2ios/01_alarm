//
//  DetailCell.swift
//  Alarm
//
//  Created by User543 on 17.05.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit

class DetailCell: UITableViewCell {
    
    @IBOutlet var title: UILabel!
    @IBOutlet var detail: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
