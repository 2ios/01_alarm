//
//  NameAlarmViewController.swift
//  Alarm
//
//  Created by User543 on 16.05.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit

protocol NameAlarmDelegate: NSObjectProtocol {
    func nameAlarm(string: String) -> Void
}

class NameAlarmViewController: UIViewController, UITextFieldDelegate {
    
    weak open var delegate: NameAlarmDelegate?
    
    @IBOutlet weak var nameField: UITextField!
    
    var nameString : String?
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.navigationController?.popViewController(animated: true)
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nameField.text = nameString
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        delegate?.nameAlarm(string: nameField.text!)
    }
}
