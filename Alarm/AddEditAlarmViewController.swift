//
//  AddAlarmViewController.swift
//  Alarm
//
//  Created by User543 on 16.05.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit
import CoreData

class AddEditAlarmViewController: UIViewController {
    
    var editAdd: Bool = false
    var indexRow: Int = 0
    var nameAlarm: String = "Будильник"
    var repeatDay: String = "Никогда"
    var songAlarm: String = "Не выбран"
    
    // Allow Delegate Data Core
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    // Initialization of AlarmCoreData
    var alarms: [AlarmData] = []
    
    @IBOutlet weak var datePicker: UIDatePicker!
    
    //If cancelButton is pressed do nothing
    @IBAction func cancelButton(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBOutlet weak var deleteAlarmOutlet: UIButton!
    
    @IBAction func deleteAlarm(_ sender: UIButton) {
        let alarmCell = alarms[indexRow]
        context.delete(alarmCell)
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
        dismiss(animated: true, completion: nil)
    }
    //If saveAlarm is pressed, save all data in Data Core
    @IBAction func saveAlarm(_ sender: UIBarButtonItem) {
        
        if editAdd == false {
        // Allow Delegate Data Core
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let alarm = AlarmData(context: context)
        alarm.timeAlarm = DateFormatter.localizedString(from: datePicker.date, dateStyle: DateFormatter.Style.none, timeStyle: DateFormatter.Style.short)
        alarm.timeAlarmDate = datePicker.date as NSDate
        alarm.daysOfAlarm = (childViewControllers[0] as? MenuTableAlarmViewController)?.repeatDayViewCell.detail.text
        alarm.nameAlarm = (childViewControllers[0] as? MenuTableAlarmViewController)?.nameViewCell.detail.text
        alarm.songAlarm = (childViewControllers[0] as? MenuTableAlarmViewController)?.songViewCell.detail.text
        alarm.repeatAlarm = ((childViewControllers[0] as? MenuTableAlarmViewController)?.repeatSongViewCell.repeatAlarmSwitch.isOn)!
        alarm.turnOnAlarm = true
        alarm.selectDays = ((childViewControllers[0] as? MenuTableAlarmViewController)?.array)! as NSArray
            
        } else {
            let alarm = alarms[indexRow]
            alarm.timeAlarm = DateFormatter.localizedString(from: datePicker.date, dateStyle: DateFormatter.Style.none, timeStyle: DateFormatter.Style.short)
            alarm.timeAlarmDate = datePicker.date as NSDate
            alarm.daysOfAlarm = (childViewControllers[0] as? MenuTableAlarmViewController)?.repeatDayViewCell.detail.text
            alarm.nameAlarm = (childViewControllers[0] as? MenuTableAlarmViewController)?.nameViewCell.detail.text
            alarm.songAlarm = (childViewControllers[0] as? MenuTableAlarmViewController)?.songViewCell.detail.text
            alarm.repeatAlarm = ((childViewControllers[0] as? MenuTableAlarmViewController)?.repeatSongViewCell.repeatAlarmSwitch.isOn)!
            alarm.turnOnAlarm = true
            alarm.selectDays = ((childViewControllers[0] as? MenuTableAlarmViewController)?.array)! as NSArray
        }
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
        self.dismiss(animated: true, completion: nil)
    }
    
    func sendNameAlarm(withname: String) -> Void {
        nameAlarm = withname
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getData()
        
        if editAdd {
            let alarm = alarms[indexRow]
            datePicker.date = alarm.timeAlarmDate! as Date
            (childViewControllers[0] as? MenuTableAlarmViewController)?.repeatDayViewCell.detail.text = alarm.daysOfAlarm
            (childViewControllers[0] as? MenuTableAlarmViewController)?.nameViewCell.detail.text = alarm.nameAlarm
            (childViewControllers[0] as? MenuTableAlarmViewController)?.songViewCell.detail.text = alarm.songAlarm
            (childViewControllers[0] as? MenuTableAlarmViewController)?.repeatSongViewCell.repeatAlarmSwitch.isOn = alarm.repeatAlarm
        } else {
            datePicker.date = Date()
            (childViewControllers[0] as? MenuTableAlarmViewController)?.repeatDayViewCell.detail.text = repeatDay
            (childViewControllers[0] as? MenuTableAlarmViewController)?.nameViewCell.detail.text = nameAlarm
            (childViewControllers[0] as? MenuTableAlarmViewController)?.songViewCell.detail.text = songAlarm
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if editAdd {
             self.navigationItem.title = "Переставить"
                deleteAlarmOutlet.isHidden = false
        } else {
            self.navigationItem.title = "Добавить"
        }
    }
    // MARK: getData function of CoreData
    func getData() {
        do {
            alarms = try context.fetch(AlarmData.fetchRequest())
        } catch {
            print("Неудачная загрузка файлов из CoreData")
        }
    }
}

