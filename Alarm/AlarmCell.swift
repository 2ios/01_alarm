//
//  AlarmCell.swift
//  Alarm
//
//  Created by User543 on 18.05.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit

class AlarmCell: UITableViewCell {

    @IBOutlet var title: UILabel!
    @IBOutlet var detail: UILabel!
    @IBOutlet var isOnSwitch: UISwitch!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
